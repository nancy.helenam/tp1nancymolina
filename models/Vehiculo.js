module.exports = (sequelize, Sequelize) => {

    const Vehiculo = sequelize.define('Vehiculo', {
        marca: Sequelize.STRING,
        modelo: Sequelize.STRING,
        precio: Sequelize.INTEGER,
        color: Sequelize.STRING
    });

    return Vehiculo;
};