const Sequelize = require('sequelize');
const Empleado = require('./Empleado');
const Sucursal = require('./Sucursal');
const Vehiculo = require('./Vehiculo');

const db = {};

 const sequelize = new Sequelize('mainDb', null, null, {
     dialect: "sqlite",
     storage: "./mainDB.sqlite"
 });

 sequelize.authenticate()
    .then(function () { console.log('Sequelize Autenticado'); })
    .catch(function (err) { console.log('Error autenticando: ' + err); })

db.Empleado = Empleado(sequelize, Sequelize);
db.Sucursal = Sucursal(sequelize,Sequelize);
db.Vehiculo = Vehiculo(sequelize,Sequelize);

db.Empleado.belongsTo(db.Sucursal);
db.Vehiculo.belongsTo(db.Sucursal);
db.Sucursal.hasMany(db.Empleado);
db.Sucursal.hasMany(db.Vehiculo);

sequelize.sync()
    .then(function () {
        console.log('Modelo sincronizado.');
    });

module.exports = db;