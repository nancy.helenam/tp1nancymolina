module.exports = (sequelize, Sequelize) => {

    const Empleado = sequelize.define('Empleado', {
        nombre: Sequelize.STRING,
        apellido: Sequelize.STRING,
        cargo: Sequelize.STRING,
        antiguedad: Sequelize.INTEGER
    });

    return Empleado;
};