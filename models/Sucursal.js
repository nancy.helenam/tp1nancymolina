module.exports = (sequelize, Sequelize) => {

    const Sucursal = sequelize.define('Sucursal', {
        nombre: Sequelize.STRING,
        direccion: Sequelize.STRING,
    });

    return Sucursal;
};