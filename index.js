const express = require('express');
const bodyParser = require('body-parser');

const routerVehiculos = require('./routes/VehiculoR');
const routerSucursales = require('./routes/SucursalR');
const routerEmpleados = require ('./routes/EmpleadoR');

const db = require('./models');

const app = express();

app.use(bodyParser.json());
app.use('/empleados', routerEmpleados);
app.use('/sucursales', routerSucursales);
app.use('/vehiculos', routerVehiculos)

app.use(express.static('public'))

app.listen(port=5000, () => {
    console.log('Servidor escuchando...');
});