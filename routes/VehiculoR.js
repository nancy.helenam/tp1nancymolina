const express = require('express');
const router = express.Router();
const models = require('../models');

function vehiculoInvalido(marca,modelo,precio,color,sucursalid) {
    return !marca || !modelo || !precio || !color || !sucursalid
}

router.get('/', (req, res) => {
    models.Vehiculo.findAll()
        .then((rows) => {
            res.send(rows);
        });
});

router.get('/:id', (req, res) => {
    const id = req.params.id;

    models.Vehiculo.findOne({
        where: {
            id: id
        }
    })
        .then((row) => {
            res.send(row);
        });
});

router.post('/', (req, res) => {
    const marca = req.body.marca;
    const modelo = req.body.modelo;
    const precio = req.body.precio;
    const color = req.body.color;
    const sucursalid = req.body.sucursalid;

    if (vehiculoInvalido(marca, modelo, precio, color, sucursalid)) {
        res.sendStatus(400)
    }

    else {


        models.Sucursal.findOne({
            where: {
                id: sucursalid
            }
        })
            .then((sucursal) => {
                models.Vehiculo.create({
                    marca: marca,
                    modelo: modelo,
                    precio: precio,
                    color: color
                }).then((vehiculo) => {

                    vehiculo.setSucursal(sucursal);

                    res.status(201).json(vehiculo);
                })
            })
    }
});

router.put('/:id', (req, res) => {
    const id = req.params.id;
    const marca = req.body.marca;
    const modelo = req.body.modelo;
    const precio = req.body.precio;
    const color = req.body.color;

    models.Vehiculo.update(
        {
            marca: marca,
            modelo: modelo,
            precio: precio,
            color: color
        },
        {
            where: { id: id }
        }
    ).then(() => {
        res.sendStatus(200);
    })
});

router.delete('/:id', (req, res) => {
    let id = req.params.id;

    models.Vehiculo.destroy({
        where: {
            id: id
        }
    }).then(() => {
        res.sendStatus(200);
    })
});

module.exports = router;