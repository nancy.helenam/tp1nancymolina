const express = require('express');
const router = express.Router();
const models = require('../models');

function sucursalIvalida(nombre, direccion) {
    return nombre === null || direccion === null
}

router.get('/', (req, res) => {
    models.Sucursal.findAll({ include: [models.Empleado, models.Vehiculo] })
        .then((rows) => {
            res.send(rows);
        });
});

router.get('/:id', (req, res) => {
    let id = req.params.id;

    models.Sucursal.findOne({
        where: {
            id: id
        },
        include: [models.Empleado, models.Vehiculo]
    })
        .then((row) => {
            res.send(row);
        });
});

router.post('/', (req, res) => {
    const nombre = req.body.nombre;
    const direccion = req.body.direccion;

    if (sucursalIvalida(nombre, direccion)) {
        res.sendStatus(400)
    }
    else {


        models.Sucursal.create({
            nombre: nombre,
            direccion: direccion,

        }).then((sucursal) => {


            res.status(201).json(sucursal);
        })

    }
});

router.put('/:id', (req, res) => {
    const id = req.params.id;
    const nombre = req.body.nombre;
    const direccion = req.body.direccion;


    models.Sucursal.update(
        {
            nombre: nombre,
            direccion: direccion
        },
        {
            where: { id: id }
        }
    ).then(() => {
        res.sendStatus(200);
    })
});

router.delete('/:id', (req, res) => {
    const id = req.params.id;

    models.Empleado.destroy({
        where: {
            SucursalId: id
        }
    }).then(() => {
        return models.Vehiculo.destroy({
            where: {
                SucursalId: id
            }
        })
    }).then(() =>

        models.Sucursal.destroy({
            where: {
                id: id
            },

        }).then(() => {
            res.sendStatus(200);
        })
    )
});

module.exports = router;