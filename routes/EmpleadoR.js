const express = require('express');
const router = express.Router();
const models = require('../models');

function empleadoInvalido(nombre, apellido, cargo, antiguedad, sucursalid) {
    return !nombre || !apellido  || !cargo  || !antiguedad || !sucursalid 
}

router.get('/', (req, res) => {
    models.Empleado.findAll()
        .then((rows) => {
            res.send(rows);
        });
});

router.get('/:id', (req, res) => {
    let id = req.params.id;

    models.Empleado.findOne({
        where: {
            id: id
        }
    })
        .then((row) => {
            res.send(row);
        });
});

router.post('/', (req, res) => {
    const nombre = req.body.nombre;
    const apellido = req.body.apellido;
    const cargo = req.body.cargo;
    const antiguedad = req.body.antiguedad;
    const sucursalid = req.body.sucursalid;

    if (empleadoInvalido(nombre, apellido, cargo, antiguedad, sucursalid)) {

        res.sendStatus(400)

    } else {

        models.Sucursal.findOne({
            where: {
                id: sucursalid
            }
        })
            .then((sucursal) => {
                models.Empleado.create({
                    nombre: nombre,
                    apellido: apellido,
                    cargo: cargo,
                    antiguedad: antiguedad
                }).then((empleado) => {

                    empleado.setSucursal(sucursal);

                    res.status(201).json(empleado);
                })
            })
    }
});

router.put('/:id', (req, res) => {
    const id = req.params.id;
    const nombre = req.body.nombre;
    const apellido = req.body.apellido;
    const cargo = req.body.cargo;
    const antiguedad = req.body.antiguedad;

    models.Empleado.update(
        {
            nombre: nombre,
            apellido: apellido,
            cargo: cargo,
            antiguedad: antiguedad
        },
        {
            where: { id: id }
        }
    ).then(() => {
        res.sendStatus(200);
    })
});

router.delete('/:id', (req, res) => {
    const id = req.params.id;

    models.Empleado.destroy({
        where: {
            id: id
        }
    }).then(() => {
        res.sendStatus(200);
    })
});

module.exports = router;